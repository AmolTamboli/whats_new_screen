//
//  ViewController.swift
//  Creats_Whats_New_Screen
//
//  Created by Amol Tamboli on 25/06/20.
//  Copyright © 2020 Amol_Tamboli. All rights reserved.
//

import UIKit
import WhatsNewKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        let whatsNew = WhatsNew(title: "WhatsNew in application", items: [
        
            WhatsNew.Item(title: "Set Alarm", subtitle: "You can set alarm in this version", image: UIImage(systemName: "alarm")),
            WhatsNew.Item(title: "Set Volume", subtitle: "You can set alarm volume", image: UIImage(systemName: "volume")),
            WhatsNew.Item(title: "Add BookMark", subtitle: "Add bookmark in description", image: UIImage(systemName: "bookmark")),
            WhatsNew.Item(title: "Play Music", subtitle: "You can play music in this version", image: UIImage(systemName: "play")),
            WhatsNew.Item(title: "Pause Music", subtitle: "You can pause the music", image: UIImage(systemName: "pause"))
        ])
        
//        guard let vc = WhatsNewViewController(whatsNew: whatsNew, versionStore: KeyValueWhatsNewVersionStore()) else {
//            return
//        }
        
        let vc = WhatsNewViewController(whatsNew: whatsNew, theme: .darkRed)
        present(vc, animated: true, completion: nil)
    }

}

